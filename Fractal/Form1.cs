﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace Fractal
{

    public partial class Form1 : Form
    {
        private const int MAX = 256;      // max iterations
        private const double SX = -2.025; // start value real
        private const double SY = -1.125; // start value imaginary
        private const double EX = 0.6;    // end value real
        private const double EY = 1.125;  // end value imaginary
        private static int x1, y1, xs, ys, xe, ye;
        private static double xstart, ystart, xende, yende, xzoom, yzoom;
        private static bool action, rectangle;
        private static float xy;
        private Image picture;
        private Graphics g1;
        private Image boxPicture;
        private Graphics boxGraphics;
        private HSB HSBcol = new HSB();

        public Form1()
        {
            InitializeComponent();

            //call starting functions
            init();
            start();
            
            pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(Form1_MouseDown);
            pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(Form1_MouseUp);
            pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(Form1_MouseMove);
        }

        public void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (action)
            {
                xe = e.X;
                ye = e.Y;

                updateBox();
            }
        }

        public void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            int z, w;

            if (action)
            {
                xe = e.X;
                ye = e.Y;
                if (xs > xe)
                {
                    z = xs;
                    xs = xe;
                    xe = z;
                }
                if (ys > ye)
                {
                    z = ys;
                    ys = ye;
                    ye = z;
                }
                w = (xe - xs);
                z = (ye - ys);
                if ((w < 2) && (z < 2)) initvalues();
                else
                {
                    if (((float)w > (float)z * xy)) ye = (int)((float)ys + (float)w / xy);
                    else xe = (int)((float)xs + (float)z * xy);
                    xende = xstart + xzoom * (double)xe;
                    yende = ystart + yzoom * (double)ye;
                    xstart += xzoom * (double)xs;
                    ystart += yzoom * (double)ys;
                }
                xzoom = (xende - xstart) / (double)x1;
                yzoom = (yende - ystart) / (double)y1;
                rectangle = false;
                mandelbrot();
            }
        }

        public void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            if (action)
            {
                rectangle = true;

                xs = e.X;
                ys = e.Y;
            }
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(picture, 0, 0);
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            mandelbrot();
        }

        public void init() // all instances will be prepared
        {
            this.Size = new System.Drawing.Size(700, 700);

            x1 = this.Size.Width;
            y1 = this.Size.Height;
            xy = (float)x1 / (float)y1;

            picture = new Bitmap(x1, y1);
            g1 = Graphics.FromImage(picture);

            boxPicture = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            boxGraphics = Graphics.FromImage(boxPicture);
        }

        public void start()
        {
            action = false;
            initvalues();
            xzoom = (xende - xstart) / (double)x1;
            yzoom = (yende - ystart) / (double)y1;
            mandelbrot();
        }

        public void stop()
        {
        }

        public void updateBox()
        {
            boxPicture = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            boxGraphics = Graphics.FromImage(boxPicture);

            if (rectangle)
            {
                 Pen pen = new Pen(Color.White);
                
                 if (xs < xe)
                 {
                     if (ys < ye)
                        boxGraphics.DrawRectangle(pen, xs, ys, (xe - xs), (ye - ys));
                     else
                        boxGraphics.DrawRectangle(pen, xs, ye, (xe - xs), (ys - ye));
                 }
                 else
                 {
                     if (ys < ye)
                        boxGraphics.DrawRectangle(pen, xe, ys, (xs - xe), (ye - ys));
                     else
                        boxGraphics.DrawRectangle(pen, xe, ye, (xs - xe), (ys - ye));
                 }
                 pictureBox1.Image = boxPicture;
            } else
            {
                pictureBox1.Image = boxPicture;
            }
        }

        private void mandelbrot() // calculate all points
        {
            int x, y;
            float h, b, alt = 0.0f;

            action = false;
            //setCursor(c1);
            toolStripStatusLabel1.Text = "Mandelbrot-Set will be produced - please wait...";

            Pen pen = new Pen(Color.Red);
            Color color;

            for (x = 0; x < x1; x += 2)
                for (y = 0; y < y1; y++)
                {
                    h = pointcolour(xstart + xzoom * (double)x, ystart + yzoom * (double)y); // color value


                    if (h != alt)
                    {
                        b = 1.0f - h * h; // brightnes

                        HSBcol.fromHSB(h * 255, 0.8f * 255, b * 255);
                        int red = 0, green = 0, blue = 0;

                        if (trackBar1.Value == 1)
                        {
                            red = Convert.ToInt32(HSBcol.rChan);
                            green = Convert.ToInt32(HSBcol.gChan);
                            blue = Convert.ToInt32(HSBcol.bChan);
                        } else if (trackBar1.Value == 2)
                        {
                            green = Convert.ToInt32(HSBcol.rChan);
                            blue = Convert.ToInt32(HSBcol.gChan);
                            red = Convert.ToInt32(HSBcol.bChan);
                        }
                        else if (trackBar1.Value == 3)
                        {
                            blue = Convert.ToInt32(HSBcol.rChan);
                            red = Convert.ToInt32(HSBcol.gChan);
                            green = Convert.ToInt32(HSBcol.bChan);
                        }
                        else if (trackBar1.Value == 4)
                        {
                            blue = Convert.ToInt32(HSBcol.rChan);
                            green = Convert.ToInt32(HSBcol.gChan);
                            red = Convert.ToInt32(HSBcol.bChan);
                        }
                        else if (trackBar1.Value == 5)
                        {
                            red = Convert.ToInt32(HSBcol.rChan);
                            blue = Convert.ToInt32(HSBcol.gChan);
                            green = Convert.ToInt32(HSBcol.bChan);
                        }
                        else if (trackBar1.Value == 6)
                        {
                            green = Convert.ToInt32(HSBcol.rChan);
                            red = Convert.ToInt32(HSBcol.gChan);
                            blue = Convert.ToInt32(HSBcol.bChan);
                        }

                        color = Color.FromArgb(red, green, blue);
                        pen = new Pen(color, 1);

                        //djm 
                        alt = h;
                    }

                    Point p1 = new Point(x, y);
                    Point p2 = new Point(x + 1, y);
                    g1.DrawLine(pen, p1, p2);
                }
            toolStripStatusLabel1.Text = "Mandelbrot-Set ready - please select zoom area with pressed mouse.";

            action = true;

            Invalidate();
        }

        private float pointcolour(double xwert, double ywert) // color value from 0.0 to 1.0 by iterations
        {
            double r = 0.0, i = 0.0, m = 0.0;
            int j = 0;

            while ((j < MAX) && (m < 4.0))
            {
                j++;
                m = r * r - i * i;
                i = 2.0 * r * i + ywert;
                r = m + xwert;
            }

            return (float)j / (float)MAX;
        }

        private void initvalues() // reset start values
        {
            xstart = SX;
            ystart = SY;
            xende = EX;
            yende = EY;
            if ((float)((xende - xstart) / (yende - ystart)) != xy)
                xstart = xende - (yende - ystart) * (double)xy;
        }

        public String getAppletInfo()
        {
            return "fractal.class - Mandelbrot Set a Java Applet by Eckhard Roessel 2000-2001";
        }

        //save image
        private void imageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.FileName = "newImage.jpg";

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                picture.Save(saveFileDialog1.FileName, ImageFormat.Jpeg);
            }
        }

        //save instance
        private void instanceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            XElement insta = new XElement("mandle",
                new XElement("xstart", xstart),
                new XElement("ystart", ystart),
                new XElement("xzoom", xzoom),
                new XElement("yzoom", yzoom)
            );

            saveFileDialog1.FileName = "newInstance.txt";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                insta.Save(saveFileDialog1.FileName);
            }
        }

        private void loadInstanceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                XmlTextReader reader = new XmlTextReader(openFileDialog1.FileName);
                string name = "";
                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element: // The node is an element.
                            name = reader.Name;
                            break;
                        case XmlNodeType.Text: //Display the text in each element.
                            Console.WriteLine("Set " + name + " to " + reader.Value);
                            if (name == "xstart")
                            {
                                xstart = XmlConvert.ToDouble(reader.Value);
                            }
                            else if (name == "ystart")
                            {
                                ystart = XmlConvert.ToDouble(reader.Value);
                            }
                            else if (name == "xzoom")
                            {
                                xzoom = XmlConvert.ToDouble(reader.Value);
                            }
                            else if (name == "yzoom")
                            {
                                yzoom = XmlConvert.ToDouble(reader.Value);
                            }
                            break;
                    }
                }
                mandelbrot();
            }
        }
    }

    class HSB
    {//djm added, it makes it simpler to have this code in here than in the C#
        public double rChan, gChan, bChan;

        public HSB()
        {
            rChan = gChan = bChan = 0;
        }

        public void fromHSB(float h, float s, float b)
        {
            float red = b;
            float green = b;
            float blue = b;

            if (s != 0)
            {
                float max = b;
                float dif = b * s / 255f;
                float min = b - dif;

                float h2 = h * 360f / 255f;

                if (h2 < 60f)
                {
                    red = max;
                    green = h2 * dif / 60f + min;
                    blue = min;
                }
                else if (h2 < 120f)
                {
                    red = -(h2 - 120f) * dif / 60f + min;
                    green = max;
                    blue = min;
                }
                else if (h2 < 180f)
                {
                    red = min;
                    green = max;
                    blue = (h2 - 120f) * dif / 60f + min;
                }
                else if (h2 < 240f)
                {
                    red = min;
                    green = -(h2 - 240f) * dif / 60f + min;
                    blue = max;
                }
                else if (h2 < 300f)
                {
                    red = (h2 - 240f) * dif / 60f + min;
                    green = min;
                    blue = max;
                }
                else if (h2 <= 360f)
                {
                    red = max;
                    green = min;
                    blue = -(h2 - 360f) * dif / 60 + min;
                }
                else
                {
                    red = 0;
                    green = 0;
                    blue = 0;
                }
            }
            rChan = Math.Round(Math.Min(Math.Max(red, 0), 255));
            gChan = Math.Round(Math.Min(Math.Max(green, 0), 255));
            bChan = Math.Round(Math.Min(Math.Max(blue, 0), 255));

        }
    }
}
